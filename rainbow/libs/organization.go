package  libs

import (
	"encoding/json"
	"fmt"
)

type Org string

const (
	OrgContext = "@context"
	OrgTypeS   = "type"
	OrgName    = "name"
)

func DefineOrg(grr map[Org]string) map[Org]string {
	//	app.context = append(app.context, "@context")
	grr[OrgContext] = "https://www.w3.org/ns/activitystreams#Organization"
	//	app.typeS = append(app.typeS, "type")
	grr[OrgTypeS] = "Org"
	//	app.name = append(app.name, "name")
	grr[OrgName] = "SnowCrashNetwork"
	return grr
}

func EncodeOrg() {
	//var app Application
	blob := make(map[Org]string)
	grr := DefineOrg(blob)

	jsonEncodedOrg, err := json.Marshal(grr)
	if err != nil {
		fmt.Println("Error marshaling json.")
	}
	fmt.Println(string(jsonEncodedOrg))
}
