package libs

type EndpointsS struct {
	proxyUrl         string
	provideClientKey string
	signClientKey    string
	sharedInbox      []OrderedCollectionS
}
type MobileS struct {
	Str          int
	Dex          int
	Int          int
	Wis          int
	Con          int
	Tec          int
	Inventory    map[string]string
	Equipment    map[string]string
	Summary      string
	LastBork     string
	Source       map[string]interface{}
	Type         string
	Following    string
	Followers    string
	Liked        string
	Inbox        string
	Outbox       string
	Url          string
	Attachment   AttachmentS
	AttributedTo AttributedToS
	Audience     AudienceS
	Content      string
	Id           string
	Context      interface{}
	Name         NameS
	EndTime      EndTimeS
	Generator    GeneratorS
	Icon         []string
	InReplyTo    InReplyToS
	Location     LocationS
	//There is no use for this right now
	Preview string
	//
	Published string
	Replies   RepliesS
	StartTime string

	Tag               TagS
	Updated           string
	To                string
	Bto               string
	Cc                string
	Bcc               string
	MediaType         string
	Duration          string
	Position          celPosS
	Streams           CollectionS
	PreferredUsername string
	Endpoints         EndpointsS
}
type ActorS struct {
	Str          int
	Dex          int
	Int          int
	Wis          int
	Con          int
	Tec          int
	Inventory    map[int]string
	Equipment    map[string]string
	Summary      string
	LastBork     string
	Source       map[string]interface{}
	Type         string
	Following    string
	Followers    string
	Liked        string
	Inbox        string
	Outbox       string
	Url          string
	Attachment   AttachmentS
	AttributedTo AttributedToS
	Audience     AudienceS
	Content      string
	Id           string
	Context      interface{}
	Name         NameS
	EndTime      EndTimeS
	Generator    GeneratorS
	Icon         []string
	InReplyTo    InReplyToS
	Location     LocationS
	//There is no use for this right now
	Preview string
	//
	Published string
	Replies   RepliesS
	StartTime string

	Tag               TagS
	Updated           string
	To                string
	Bto               string
	Cc                string
	Bcc               string
	MediaType         string
	Duration          string
	Position          celPosS
	Streams           CollectionS
	PreferredUsername string
	Endpoints         EndpointsS
}
